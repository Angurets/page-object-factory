package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage {

    private final WebDriver driver;

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }


    @FindBy(id="menu-favorites")
    WebElement favIcon;

    @FindBy(xpath = "//span[contains(@class,'menu-element-inner')]")
    WebElement langDropDown;


    //@FindBy(id="menu-" + lang + "-locale")
    //WebElement selectedLanguage;


    public void lang1 (String lang){

       favIcon.isDisplayed();

        langDropDown.click();
        // selectedLanguage.click();

    }

}
