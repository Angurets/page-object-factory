package Pages;

import org.openqa.selenium.WebDriver;


public class CookiePage {

    private final WebDriver driver;

    public CookiePage(WebDriver driver) {
        this.driver = driver;
    }

     public void cookies(String name){
        driver.manage().getCookieNamed(name).getValue();
     }


}
