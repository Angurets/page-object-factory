package FBTesting;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;


public class HomeTask {
    WebDriver driver;
By favIcon = By.xpath("//span[contains(@class,'menu-element-inner')]");

    private void languageChanging(String lang) {
        Waiters waiters = new Waiters(driver);
        driver.get("https://www.templatemonster.com/");
        waiters.favorites(By.id("menu-favorites"));
        driver.findElement(By.xpath("//span[contains(@class,'menu-element-inner')]")).click();
        driver.findElement(By.id("menu-" + lang + "-locale")).click();


         //wait.until(ExpectedConditions.urlContains(lang.toLowerCase()));
    }




    private void waitForCookie(String name) {
        try{
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until((WebDriver webdriver) -> driver.manage().getCookieNamed(name) != null);

        } catch (org.openqa.selenium.TimeoutException e) {

            System.out.println("Cookie doesn't appear");
            driver.navigate().refresh();
        }
    }

    @BeforeMethod
    void before() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
    }

    @DataProvider(name = "Languages")
    public Object[][] listOfLanguages (){
        return new Object[][]{
                {"RU"},
                {"FR"},
                {"BR"}
        };
    }



    @Test(dataProvider = "Languages", dataProviderClass = HomeTask.class)
    void langItaly(String lang) {
        languageChanging(lang);
        Assert.assertTrue(driver.getCurrentUrl().contains(lang.toLowerCase()));
    }


    @Test
    void test1() {

        driver.get("https://www.templatemonster.com/");
        waitForCookie("aff");
        Assert.assertEquals(driver.manage().getCookieNamed("aff").getValue(), "TM");
    }

    @AfterMethod
    void after() {
        driver.quit();
    }
}
