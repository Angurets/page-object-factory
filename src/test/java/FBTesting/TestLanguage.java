package FBTesting;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class TestLanguage {
WebDriver driver;



          @BeforeMethod
    void before() throws MalformedURLException {
           driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
    }

    @DataProvider(name = "Languages")
    public Object[][] listOfLanguages (){
        return new Object[][]{
               {"RU"},
               {"FR"},
                {"BR"}
        };
    }

    @Test(dataProvider = "Languages", dataProviderClass = HomeTask.class)
            void languages(String lang){

        MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
        Waiters waiters = new Waiters(driver);
        driver.get("https://www.templatemonster.com/");
        waiters.favorites(By.id("menu-favorites"));
        mainPage.lang1(lang);
        driver.findElement(By.id("menu-" + lang + "-locale")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains(lang.toLowerCase()));}

    }







