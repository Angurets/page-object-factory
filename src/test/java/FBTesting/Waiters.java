package FBTesting;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiters {

    WebDriver driver;

    public Waiters (WebDriver driver) {
        this.driver = driver;}


    public void waitingForCookieNamed(String name){

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until((WebDriver webdriver) -> driver.manage().getCookieNamed(name) != null);
    }


    public void favorites(By id){
        WebDriverWait waitFav1 = new WebDriverWait(driver, 15);
        waitFav1.until(ExpectedConditions.visibilityOfElementLocated(By.id("menu-favorites")));
    }

}
