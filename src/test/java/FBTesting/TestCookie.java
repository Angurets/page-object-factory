package FBTesting;

import Pages.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class TestCookie {
    WebDriver driver;

    @BeforeMethod
    void before() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
    }


    @Test
    void languages(){
        Waiters waiters = new Waiters(driver);
        driver.get("https://www.templatemonster.com/");
        waiters.waitingForCookieNamed("aff");
        Assert.assertEquals(driver.manage().getCookieNamed("aff").getValue(), "TM");
}
}
